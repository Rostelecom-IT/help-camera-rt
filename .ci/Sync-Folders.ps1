param (
    [string] $sourceFolder,
    [string] $sourceExcludeList,
    [string] $destFolder,
    [string] $destExcludeList,
    [Int32]  $fileCountToClear
)

if (!(Test-Path $sourceFolder))
{
	throw "Исходная папка $sourceFolder отсутствует."
}
if (!(Test-Path $destFolder))
{
	throw "Целевая папка $destFolder отсутствует."
}

$sourceFolderFullPath = Resolve-Path $sourceFolder
$destFolderFullPath = Resolve-Path $destFolder

# Prepare selectors for Compare-Object that cuts absolute path to relative ones
$sourceFolderLength = $sourceFolderFullPath.ToString().Length
$sourceNameSelector = @{label="Name";expression={$_.FullName.ToString().Substring($sourceFolderLength)}}
$destFolderLength = $destFolderFullPath.ToString().Length
$destNameSelector = @{label="Name";expression={$_.FullName.ToString().Substring($destFolderLength)}}

# Get both folder contents with relative paths
$currentFiles = @(Get-ChildItem -Exclude ($destExcludeList -split ',') -Path "$destFolderFullPath" -ErrorAction SilentlyContinue | Get-ChildItem -Recurse | Select-Object -Property $destNameSelector)
$newFiles = @(Get-ChildItem -Exclude ($sourceExcludeList -split ',') -Path "$sourceFolderFullPath" -ErrorAction SilentlyContinue | Get-ChildItem -Recurse | Select-Object -Property $sourceNameSelector)

if (($newFiles | Measure-Object).Count -le $fileCountToClear)
{
	throw "Исходная папка содержит менее $fileCountToClear файлов."
}

# Compare both folder contents
$comparison = Compare-Object -ReferenceObject $currentFiles -DifferenceObject $newFiles -Property Name

# Remove folders and files from destination folder that should be removed.
# We use descending sort to delete files before folders thst contains it.
$comparison | Where-Object { $_.SideIndicator -eq "<=" } | Sort-Object -Property Name -Descending | foreach { $fsoName = "$destFolderFullPath$($_.Name)"; Write-Output "Trying to delete $fsoName"; Remove-Item -Force -Recurse -Path $fsoName }

# Copying all other items
# ToDo: co not copy files, that is not changed
Write-Host "Copying from '$sourceFolderFullPath\*' to '$destFolderFullPath'"
Copy-Item -Force -Recurse "$sourceFolderFullPath\*" -Destination "$destFolderFullPath"
