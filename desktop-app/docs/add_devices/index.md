# Добавление устройств

## Автоматический поиск устройств

Чтобы выполнить автоматический поиск устройств, нажмите на символ поиска и добавления устройств (1) в правом верхнем углу экрана.

![Поиск устройств в локальной сети](./images/search_local_network_desktop_3.11.1.0.png "Поиск устройств в локальной сети")

Если в результате поиска не было найдено ни одного устройства, на экране появится соответствующее сообщение.

![Отсутствие устройств в локальной сети](./images/discovery_no_devices_found_desktop_3.11.1.0.png "Отсутствие устройств в локальной сети")

Для повторения поиска нажмите на кнопку повторного поиска (2).

Если в результате поиска устройства были найдены, ПО покажет их список:

![Список устройств в локальной сети](./images/discovery_results_desktop_3.11.1.0.png "Список найденных устройств в локальной сети")

Выберите устройство для добавления, нажав кнопку +, после чего введите логин и пароль для доступа к устройству.

## Добавление устройства или RTSP-ссылки вручную

Для ручного добавления устройства или RTSP-ссылки нажмите кнопку ручного добавления (3)(кнопка доступна только в ограниченном режиме работы приложения).

![Добавление устройства вручную](./images/add_device_manually_desktop_3.11.1.0.png)

После чего из выпадающего списка выберите тип добавляемого объекта и введите соответствующие параметры. Для добавления камеры Ростелеком/VMS: имя, IP-адрес или имя хоста, логин и пароль.

![Экран ограниченного режима с диалоговым окном добавления камеры Ростелеком/VMS](./images/add_rt_camera_desktop_3.12.0.png "Диалоговое окно ручного добавления камеры Ростелеком/VMS").

Для добавления RTSP-ссылки: RTSP-ссылку.

![Экран ограниченного режима с диалоговым окном добавления камеры RTSP](./images/add_rtsp_camera_desktop_3.12.0.png "Диалоговое окно ручного добавления RTSP-ссылки").
