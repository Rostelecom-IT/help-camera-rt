## Просмотр раскладки

Чтобы попасть на экран раскладки, перейдите на экран «Раскладки» и дважды кликните на нужную.

![Экран новой раскладки с двумя добавленными камерами](./images/layout_with_cameras_desktop_3.11.1.0.png)

Попасть на экран любой раскладки можно в режимах «Облако» и «Сеть», но в режиме «Только сеть» работа с раскладками ограничена: без доступа к серверу раскладки можно просматривать, но видео будет доступно только с устройств в локальной сети.

### Просмотр раскладки и управление просмотром

Для управления просмотром служит таймлайн внизу экрана. Управление аналогично описанному в разделе «Управление просмотром камеры» и применяется ко всем камерам раскладки:

- остановка/запуск воспроизведения;
- переход к архивному контенту и к прямому эфиру;
- ускорение/замедление воспроизведения;
- перемотка.

### Режимы «Дополнительный поток» и «Ключевые кадры» при работе с раскладками

Режимы «Дополнительный поток» и «Ключевые кадры» используются, чтобы уменьшить нагрузку на процессор при воспроизведении большого числа видео, а также при плохом соединении с интернетом. Подробнее о режимах вы можете прочитать в разделе [«Видео устройства»](../camera_view/index.md).

Приложение автоматически переводит камеры в режимы «Дополнительный поток» и «Ключевые кадры», когда количество ячеек в раскладке больше, чем N и M соответственно. Числа N и M задаются с помощью настроек «Минимальное количество ячеек раскладки для запроса дополнительных потоков»  и «Минимальное количество ячеек раскладки для запроса потоков с ключевыми кадрами» в [общих настройках](../settings_app/index.md).

![Изменение минимального количества ячеек раскладки для запроса потоков с ключевыми кадрами](./images/setting_of_stream_parameters_in_layouts_desktop_3.11.1.0.png)

### Проигрывание раскладок на нескольких мониторах

С помощью функции «Новое окно», описанной в разделе [«Главное окно приложения»](../main_window/index.md), вы можете создавать несколько окон, в каждом из которых можно разместить раскладку, и перенести их на другой монитор.

Обратите внимание, что **переносить окна, в которых воспроизводится видео, с монитора на монитор не рекомендуется**. Чтобы перенести окно на другой монитор, сначала перейдите в раздел, где нет плееров (например, раздел «Настройки»), а уже затем перенесите окно и включите воспроизведение видео.
