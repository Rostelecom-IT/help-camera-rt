# Редактирование профиля

## Изменение имени и фамилии

1. Нажмите на адрес почты в верхнем правом углу окна.

    ![](img/image8.png)

2. Введите новые имя и/или фамилию.
3. Нажмите курсором за пределами полей ввода имени и фамилии.

Название организации, указанное в вашем профиле, совпадает с названием, указанным вами в договоре. Его невозможно изменить.

## Изменение пароля аккаунта

Чтобы сменить пароль аккаунта:

1. Войдите в профиль.
2. Нажмите на адрес почты в верхнем правом углу окна.
3. Нажмите «Настройка учетной записи “Ростелеком паспорт”».

    ![](img/image9.png)

4.  Перейдите во вкладку «Пароли и авторизация».

    ![](img/image10.png)

5.  Введите старый и новый пароли.
6.  Нажмите кнопку «Изменить пароль».

Пароль обязательно должен быть длиной от 8 до 20 символов и содержать латинские заглавные и строчные буквы, а также цифры и/или спецсимволы.

## Изменение часового пояса

1. Откройте окно профиля. Часовой пояс вашего аккаунта указан в окне.
2. Нажмите на поле «Часовой пояс».
3. Выберите необходимый часовой пояс из выпадающего списка.
