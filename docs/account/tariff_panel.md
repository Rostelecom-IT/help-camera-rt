# Тарифная панель

Тарифный план вашей организации содержит несколько тарифов для камер, приобретенных у поставщика услуги, и несколько — для RTSP-камер. Вы можете переключаться между тарифами на каждой камере на тарифной панели.

> Также изменить тариф камеры можно через окно её настроек.

## Составные части тарифа

Тариф камеры в первую очередь регулирует тип ее записи:

- «Полная запись» — камера передает в архив весь медиаконтент.
- «Только события» — хранится только видео, на котором камера распознала события.
- «Экономия трафика» — запись с частотой от 4 до 12 кадров в минуту.
- «Без хранения архива» — нулевой тариф, также известный как тариф Онлайн.

Для первых двух типов тариф также включает максимальный битрейт или опцию выбора между несколькими битрейтами. Для типа «Экономия трафика» битрейт установить нельзя, но тариф определяет, сколько кадров в минуту будет передаваться при таком режиме вещания. Также тариф камеры определяет глубину хранения архива от камеры в сутках, количество клипов и суммарную длительность закладок, которые можно использовать на камере. Четвертый тип тарифа — «Без хранения архива». При нём запись в архив не ведётся, пользователь может просматривать прямой эфир локально. Камера на нулевом тарифе вещает видео в платформу только в трех случаях:

1. **Пользователь смотрит прямой эфир от камеры.** Идёт просмотр — камера вещает прямой эфир, закончился просмотр — перестает.
2. **Пользователь смотрит архив от камеры.** Это локальный архив с самой камеры, который пользователь может запросить через облачные клиенты. Идёт просмотр — камера вещает архив за выбранный период, закончился просмотр — перестает.
3. **На камере произошло событие.** У многих камер есть своя собственная, встроенная в них видеоаналитика на движение и звук. Если камера фиксирует такое событие, то она шлет видео события в платформу. 

## Стоимость тарифа

Камера, использующая тариф, оплачивается каждый месяц. Стоимость тарифа указана в договоре вашей организации с поставщиком услуги. Перевод камеры с одного тарифа на другой не оплачивается.

## Установка тарифа для камеры

Для перехода к тарифной панели нажмите на иконку в виде чемодана в верхнем меню.

![](img/tariff_panel.png)

На тарифной панели отображается список всех камер, добавленных в Личный кабинет, и назначенные им тарифы.

При наведении на строку камеры в конце строки отобразится иконка настроек.

![](img/tariff_panel_camera_menu.png)

При нажатии на неё откроется окно настройки тарифа камеры.

1. Установите нужный тип записи: полная запись, запись только событий или экономия трафика. Если в тарифном плане вашей организации нет тарифа, поддерживающего нужный тип записи для настраиваемой камеры, вам не будет показана соответствующая кнопка для настройки. Если тарифный план организации поддерживает только один тип записи, то кнопок для управления типами записи не будет вообще.
2. Если вы выбрали типы записи «Полная запись» или «Только события», установите максимальный битрейт, с которым камера сможет транслировать видео.
3. Если вы выбрали тип «Экономия трафика», укажите количество кадров в минуту, которое будет вещать камера.
4. Установите глубину хранения архива.
5. Нажмите кнопку «Установить тариф».

Стоимость нового тарифа камеры будет показана справа от кнопки «Установить тариф».
