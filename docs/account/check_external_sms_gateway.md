# Проверка внешнего SMS-шлюза

Для тестирования внешнего шлюза он должен быть сохранен.

1. Откройте боковое меню.
2. Нажмите «Настройки шлюза» в разделе «SMS-уведомления».
3. Нажмите «Протестировать шлюз» в правом верхнем углу страницы.
4. Если к вашему аккаунту привязан телефонный номер, он появится в поле «На номер телефона будет отправлено тестовое SMS-сообщение». Вы можете изменить его, если требуется протестировать шлюз с помощью другого телефона. Если телефон не привязан, введите его. Ввод телефона в поле для тестирования шлюза не означает его привязку к аккаунту — введенный номер не будет сохранен в системе.
5. Нажмите «Отправить тестовое SMS».
6. Проверьте получение SMS на указанный телефонный номер.
7. Проверьте лог запроса и ответа шлюза.

Лог запроса и ответа позволяет понять, принял ли шлюз запрос на передачу SMS-сообщения и каким образом обработал его. Если вы не получили тестовое SMS-сообщение и причина этого не очевидна из лога ответа шлюза, обратитесь к технической поддержке шлюза за разъяснением.
