# Пакеты SMS-уведомлений

Чтобы получать SMS-уведомления через шлюз Ростелекома, нужно купить пакеты SMS-уведомлений. Пакет оплачивается разово — заплатив за пакет один раз, вы получаете возможность расходовать SMS в нем, пока они не закончатся или пока не истечет срок жизни пакета. После этого нужно купить новый пакет, чтобы продолжить пользоваться SMS-уведомлениями через шлюз поставщика услуги.

Вы можете купить несколько пакетов уведомлений сразу. SMS-уведомления будут расходоваться сначала из того пакета, срок жизни которого истекает раньше других. Пакеты SMS-уведомлений покупаются на организацию. Все сотрудники организации, получающие SMS-уведомления, расходуют купленные пакеты. Чтобы контролировать расход SMS-уведомлений, нужно установить лимит SMS-уведомлений для каждого сотрудника. Для владельца организации лимит устанавливать не требуется, он бесконечен.

## Списки пакетов SMS-уведомлений

Списки пакетов, которые вы можете купить и которые уже купили, доступны на странице «Пакеты». Чтобы открыть страницу, нажмите на кнопку-здание слева от имени профиля и выберите пункт «SMS-уведомления».

Для каждого пакета, доступного для заказа, указано название, срок его жизни и количество SMS-уведомлений в нем, а также цена. Для пакетов, которые уже были заказаны, указаны даты заказа и срок жизни, а также статус. SMS-уведомления расходуются из пакетов со статусом «Активный». Количество использованных SMS-уведомлений в пакете можно увидеть в колонке «Использовано SMS».

## Цена пакета

В зависимости от тарифного плана вашей организации цена пакета SMS-уведомлений может отображаться как с учётом НДС, так и без НДС. Если цена пакета отображается без НДС, то при покупке пакета НДС все равно будет начислен, о чем система вас уведомит.

## Срок жизни пакета

Срок жизни пакета начинает истекать с момента покупки. Если в аккаунте организации куплено несколько пакетов, SMS-уведомления расходуются из того, чей срок жизни истекает раньше. Когда срок жизни пакета истек, все неиспользованные SMS-уведомления в нем сгорают.

## Оповещение при истечении срока жизни пакета SMS-уведомлений

Если к аккаунту владельца организации привязан телефон, то владельцу будут приходить SMS-сообщения:

* о том, что израсходовано 80 % SMS-уведомлений организации и какое количество SMS-уведомлений осталось;
* о полностью израсходованных SMS-уведомлениях организации и необходимости купить новый пакет SMS-уведомлений, чтобы продолжить ими пользоваться.
