# Управление PTZ-камерой

Если камера поддерживает панорамирование (pan), наклон (tilt), зум (zoom) вы можете активировать режим управления PTZ-камерой.

Для этого откройте плеер камеры с прямым эфиром и в боковом меню перейдите на вкладку **«Управление PTZ»**.

## Элементы управления PTZ-камеры

При переключении в режим управления PTZ-камеры отобразится окно с прямым эфиром устройства и следующие элементы управления:

1. Шаг смещения для калибровки сдвига камеры.
2. Кнопки управления фокусировкой для ручной настройки фокусного расстояния объектива.
3. Кнопки изменения изменения уровня зума камеры.
> В режиме управления элементы 2 и 3 отображаются только для камер с вариофокальным или оптическим зумом.
4. Кнопки управления вращением камеры, позволяющие изменять положение камеры.
5. Cсылка для выхода из управления PTZ-камеры. При переходе элементы управления будут скрыты. Для возврата перейдите по ссылке **«Войти в управление»**.
6. Таймер управления камерой установлен на 60 секунд. Отсчет таймера начинается от последнего действия с камерой. После окончания времени элементы управления будут скрыты. Для возврата перейдите по ссылке **«Войти в управление»**.

![элементы управления камеры с Zoom](img/camera_control_with_zoom.png "элементы управления камеры с Zoom")  

## Управление позициями камеры

### Исходная позиция

Исходная позиция позволяет вернуть камеру в её начальное положение.

Для перехода в исходную позицию в боковом меню в списке позиций выберите **«Исходная позиция»**. Камера будет перемещена в исходное положение.

![исходная позиция](img/starting_position.png "исходная позиция") 

После выхода из режима управления камера запоминает текущую позицию как позицию по умолчанию. 

### Создание новой позиции

Для фиксации камеры в нужном положении нужно сохранить позицию.

Для этого выполните следующие действия:

1. В плеере камеры перейдите на вкладку **«Управление PTZ»**.
2. С помощью кнопок перемещения установите нужную позицию камеры.
3. В боковом меню укажите название новой позиции и нажмите кнопку **«Сохранить»**. 

Сохраненная позиция отобразится в списке бокового меню. При выборе позиции камера будет перемещена в заданное положение.

![создание новой позиции](img/creating_a_new_position.png "создание новой позиции") 

### Изменение позиции

Для изменения ранее заданной позиции выполните следующие действия:

1. С помощью кнопок перемещения установите новую позицию.
2. В боковом меню выберите в списке нужную позицию и нажмите кнопку меню ![меню](img/menu.png "меню"), после чего нажмите кнопку сохранения ![кнопка сохранения](img/save_button.png "кнопка сохранения"). Положение камеры обновится автоматически.

![изменение позиции](img/change_of_position.png "изменение позиции") 

### Переименование позиции

Для переименования позиции:

1. В боковом меню выберите в списке нужную позицию и нажмите кнопку меню ![меню](img/menu.png "меню"), после чего нажмите кнопку редактирования ![кнопка редактирования](img/edit_button.png "кнопка редактирования"). 
2. В открывшемся поле укажите новой название и нажмите на свободное поле. Название позиции будет изменено.

### Удаление позиции

Для удаления позиции в боковом меню выберите в списке нужную позицию и нажмите кнопку меню ![меню](img/menu.png "меню"), после чего нажмите кнопку удаления ![кнопка удаления](img/delete_button.png "кнопка удаления"). Позиция будет удалена из списка.