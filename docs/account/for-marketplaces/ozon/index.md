# Правильное наименование камер ПВЗ согласно требованиям OZON

После запуска услуги Видеонаблюдения в вашем ПВЗ необходимо войти в [Личный кабинет](https://lk-b2b.camera.rt.ru).

В Личном кабинете после добавления камер необходимо присвоить им названия согласно требованиям OZON. Сделать это можно в Настройках оборудования. 

Подробно интерфейс Личного кабинета описан [в соответствующем разделе Справочного центра](../../interface.md). 

Чтобы перейти к настройкам камеры, необходимо навести курсор мыши на её превью и нажать на шестерёнку, расположенную в правом нижнем углу:

![«Переход к настройкам камеры»](./img/ozon_go-to-camera-settings.png)

 Вы попадете в меню настроек камеры и сможете поменять ее название:

![«Настройки камеры»](./img/ozon_camera-settings.png)

Для названия камер необходимо использовать следующие шаблоны: 

  * Складская зона

    Наименование камеры: `ПВЗ_склад`

    Примеры:

     * `МОСКВА_4000_склад`
     * `КОМСОМОЛЬСК_НА_АМУРЕ_100_склад`

  * Клиентская зона

    Наименование камеры: `ПВЗ_клиентская_зона`

    Примеры:

     * `САРАТОВ_75_клиентская_зона`
     * `РОСТОВ_НА_ДОНУ_100_клиентская_зона`

!!! warning

    Обязательно назовите камеры строго по образцу. Не выбирайте другие названия, даже похожие, и не используйте латинские буквы. Иначе это осложнит подключение и поиск вашей камеры в наших системах. Если камера не будет найдена из-за некорректного названия и наши специалисты не смогут просмотреть видеозапись нужного процесса, вам нужно будет самостоятельно выгружать запись с камер и отправить ее, как раньше.

# Предоставление доступа к камерам ПВЗ для OZON

Если вы успешно завершили все процессы и этапы подключения, настройки и проверки камер в ПВЗ, необходимо предоставить доступ к камерам в Личный Кабинет OZON. Для этого необходимо в Личном Кабинете перейти в меню настроек камеры - необходимо навести курсор мыши на её превью и нажать на шестерёнку, расположенную в правом нижнем углу. Вы попадете в меню настроек камеры и перейти к настройкам «[Предоставление доступа к просмотру камеры по электронной почте](../../share_live_url_via_email.md)». 

В данном меню необходимо выполнить действия по передаче прав на камеры в адрес аккаунта OZON с адресом электронной почты [video_ozon@ozon.ru](mailto:video_ozon@ozon.ru).

**Отправка приглашения**

1.	Откройте окно настроек камеры. 
1.	Нажмите «Поделиться трансляцией».
1.	Включите переключатель «Открытый доступ». Если переключатель неактивен, то тарифный план организации не включает эту услугу.
1.	Введите почту пользователя `video_ozon@ozon.ru` в поле «Доступ по электронной почте».
1.	Повторно отправить приглашение можно будет не ранее, чем через минуту.
1.	После этого на адрес аккаунта OZON уйдет письмо с приглашением пользователя к просмотру камеры.

Рекомендуется скопировать эту почту (аккаунт) с данной страницы и вставить в нужное поле в настройках функции «Поделиться трансляцией». 