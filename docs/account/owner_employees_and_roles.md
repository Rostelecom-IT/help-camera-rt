# Владелец, сотрудники и их роли

Пользователи B2B-сервиса делятся на два типа: владелец организации и сотрудники организации.

## Владелец организации и администраторы корпоративного аккаунта

Владельцем организации называется пользователь, который работает в корпоративном аккаунте. В этом аккаунте доступна вся функциональность системы без ограничений, нет лимита на SMS-уведомления, возможно подключить внешний SMS-шлюз и управлять аккаунтами сотрудников.

## Роли сотрудников

Первые аккаунты сотрудников создает владелец организации. Последующие могут создавать и другие сотрудники с правом управлять учетными записями сотрудников.

Владелец может назначить некоторых сотрудников администраторами и предоставить им практически те же права и привилегии, что и у себя. Но также сотрудники могут быть и очень ограничены в использовании функциональности сервиса. Например, если владелец или администратор корпоративного аккаунта назначит сотруднику роль «Только просмотр», то сотрудник не сможет делать в аккаунте ничего, кроме просмотра видео от камер.

## Описание ролей

| **Роль**                                          | **Описание роли**                                              |
|---------------------------------------------------|----------------------------------------------------------------|
| **Администратор (доступно все)**                  | Доступны все функции                                           |
| **Только просмотр (live/архив)**                  | Просмотр live-трансляции камеры, скриншот, просмотр архива камеры, смена своего пароля, управление своим профилем |
| **Оператор (расширенные права)**                  | PTZ, создание mp4-клипа, изменение качества видео, настройка слепых зон, изменение настройки датчиков, изменение параметра «название и время», изменение часового пояса камеры/регистратора, поворот изображения, привилегии приложения для ПК, просмотр live-трансляции камеры, скриншот, просмотр архива камеры, просмотр трансляции расшаренных камер, редактирование имени камеры, скачивание mp4 с камеры, смена своего пароля, управление настройками слежения за объектом, управление облачными хранилищами, управление регистраторами, управление своим профилем, управление тэгами камер, установка закладок |
| **Оператор (только просмотр трансляции)**         | PTZ, включать/выключать открытый доступ, создание mp4-клипа с камеры, изменение качества видео, настройка слепых зон, изменение настройки датчиков, изменение параметра «название и время», изменение часового пояса камеры/регистратора, поворот изображения, привилегии приложения для ПК, просмотр live-трансляции камеры, скриншот, просмотр архива камеры, просмотр трансляции расшаренных камер, просмотр учётных записей сотрудников, редактирование имени камеры, редактирование координаты камеры, скачивание mp4 с камеры, смена своего пароля, управление настройками слежения за объектом, управление облачными хранилищами, управление регистраторами, управление своим профилем, управление сотрудниками, управление тэгами камер, установка закладок |
| **Администратор (сотрудники)**                    | PTZ, включать/выключать открытый доступ, создание mp4-клипа с камеры, изменение качества видео, настройка слепых зон, изменение настройки датчиков, изменение параметра «название и время», изменение часового пояса камеры/регистратора, поворот изображения, привилегии приложения для ПК, просмотр live-трансляции камеры, скриншот, просмотр архива камеры, просмотр трансляции расшаренных камер, просмотр учётных записей сотрудников, редактирование имени камеры, редактирование координаты камеры, скачивание mp4 с камеры, смена своего пароля, управление настройками слежения за объектом, управление облачными хранилищами, управление регистраторами, управление своим профилем, управление сотрудниками, управление тэгами камер, установка закладок |
| **Оператор (live) Просмотр видео** | Привилегии приложения для ПК, просмотр live-трансляции камеры, скриншот |
| **Оператор (просмотр/клипы/закладки)**            | PTZ, создание mp4-клипа с камеры, поворот изображения, привилегии приложения для ПК, просмотр live-трансляции камеры, скриншот, просмотр архива камеры, просмотр расшаренных камер, регистрация новых камер, скачивание mp4-клипа с камеры, смена своего пароля, управление настройками слежения за объектом, управление своим профилем, установка закладок |
| **Оператор-настройщик камер**                     | Предоставление доступа, включение/выключение записи, включение/выключение микрофона, создание mp4-клипа с камеры, изменение качества видео, настройка слепых зон, изменение настройки датчиков, изменение настройки тепловизора, изменение параметра «название и время», изменение часового пояса камеры/регистратора, поворот изображения, привилегии приложения для ПК, live-трансляции камеры, скриншот, просмотр архива камеры, регистрация новых камер, редактирование имени камеры, редактирование координаты камеры, скачивание mp4-клипа с камеры, смена своего пароля, управление камерами и группами камер, управление своим профилем, установка закладок |
