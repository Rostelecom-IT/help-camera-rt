# Управление сотрудниками

![](./img/image38.jpg)

На экране «Управление сотрудниками» отображается список электронных
адресов сотрудников и количество камер, которое привязано к каждому
сотруднику.

В разделе «Управление сотрудниками» можно:

- добавить сотрудника,
- назначить или сменить роль сотруднику,
- выбрать камеры, которые будут доступны сотруднику,
- удалить сотрудника.

## Добавление сотрудника

![](./img/image39.jpg)

В профиле в разделе «Управление сотрудниками» нажмите на кнопку «Создать
сотрудника». Откроется экран добавления сотрудника. Введите данные для
нового сотрудника:

- его электронную почту;
- пароль;
- фамилию и имя;
- выберите роль для сотрудника;
- выберите камеры, к которым должен быть доступ у сотрудника, и нажмите «Сохранить».

Нажмите на галочку в правом верхнем углу экрана, чтобы завершить
добавление сотрудника. Новый сотрудник появится в списке.
Передайте ему пароль, чтобы он мог зайти в приложение с помощью
своей почты.

## Изменение данных существующего сотрудника

Чтобы изменить доступ существующего сотрудника к камерам или сменить его
роль, перейдите в раздел «Управление сотрудниками», нажмите на почту
нужного сотрудника в списке. Откроется карточка сотрудника. В
карточке сотрудника можно изменить:

- имя и фамилию;
- роль;
- набор камер, доступных сотруднику.

## Удаление сотрудника

Чтобы удалить сотрудника, выберите его почту в списке сотрудников и
нажмите «Удалить сотрудника» внизу страницы.
