# Передача голоса на камеру

Функция передачи голоса доступна только для камер, у которых есть
динамик.

1.  Перейдите к просмотру прямого эфира.
2.  Нажмите и удерживайте кнопку ![](./img/image24.png) слева над плеером.
3.  Говорите в микрофон мобильного устройства.
4.  Камера воспроизведет ваш голос через динамик.
5.  Отпустите кнопку с микрофоном, чтобы остановить передачу голоса.
