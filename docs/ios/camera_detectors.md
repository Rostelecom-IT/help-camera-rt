# Настройка датчиков камеры

Чтобы перейти к настройке датчиков, с помощью которых камера детектирует
события, нажмите кнопку «Настройка датчиков».

## Детектор звука

Детектор звука обнаруживает события звука рядом с камерой. Чтобы
детектор работал, микрофон камеры должен быть включен. Если у
камеры нет микрофона, настройка детектора недоступна.

Чтобы включить или выключить детектирование событий звука, нажмите на
переключатель «Детектор звука».

С помощью ползунка установите чувствительность детектора.

## Пересечение линии

Детектирование пересечения линии возможно только тогда, когда камера
оборудована детектором движения.

Чтобы открыть экран настройки детектирования пересечения линии, нажмите
кнопку «Пересечение линии». На кнопке написан статус «Активно», если
детектирование пересечения линии настроено и включено, и «Не
настроено», если детектирование выключено.

![](./img/image29.jpg)

Чтобы включить или выключить детектирование пересечения линии, нажмите
на переключатель «Пересечение линии».

Установите линию на скриншоте с камеры. Перемещайте линию по скриншоту,
растягивайте и сжимайте ее за белые круги на концах отрезка. Области по
сторонам от линии отмечены буквами «A» и «B», чтобы можно было указать
направление ее пересечения.

С помощью кнопок в блоке «Направление движения» установите направление
пересечения линии, которое будет детектироваться как событие. Можно
указать направление от A к B, от B к A или оба направления сразу.

С помощью ползунка установите чувствительность детектора.

Если детектор пересечения линии включен и настроен, то камера фиксирует
события пересечения линии. Они доступны в списке событий камеры.

## Датчик движения

Датчик движения обнаруживает, что в кадре камеры появилось движение.
Также он используется детекторами пересечения линии и вторжения в
зону. Если камера не оборудована детектором движения, она не сможет
детектировать события движения, движения в зоне и пересечения линии.

Чтобы включить или выключить детектирование движения во всей области
кадра камеры, нажмите на переключатель «Датчик движения».

Выберите чувствительность детектора движения.

Если детектор движения включен, то камера фиксирует события движения.
Они доступны в списке событий камеры.

## ИК-датчик движения

Если на камере есть ИК-датчик движения, его можно включить и выключить с
помощью переключателя в настройках датчиков камеры.

Если детектор движения включен, то камера фиксирует события движения в
темноте. Они доступны в списке событий камеры.

## Движение в зонах

Детектирование движения в зонах возможно только тогда, когда камера
оборудована детектором движения.

Чтобы открыть экран настройки детектирования движения в настроенных
зонах в кадре, нажмите на кнопку «Движение в зонах». На кнопке
написан статус «Активно», если детектирование настроено и включено,
и «Не настроено», если детектирование выключено.

![](./img/image30.jpg)

Чтобы включить или выключить детектирование движения в зонах, нажмите на
выключатель «Приоритетное уведомление» на экране «Движение в зонах».
Приоритетное уведомление означает, что при хотя бы одной настроенной
зоне камера будет детектировать движение в ней, а не во всем кадре.

Вы можете настроить до 3 зон, в которых будет детектироваться движение.
Каждая зона имеет свой цвет: желтый, синий и зеленый. Все зоны —
прямоугольники. Они могут накладываться друг на друга и совпадать
границами.

Чтобы добавить новую зону, нажмите кнопку «+» в правом верхнем углу
экрана.

Изменяйте размеры зоны, перемещая белые круги на ее углах. Чтобы
переместить зону в другое место кадра, нажмите на нее и
перетяните в нужное место. Для каждой зоны можно задать свою
чувствительность датчика.

Чтобы удалить зону:

1.  Нажмите на зону, чтобы она стала доступной для редактирования.
2.  Нажмите на кнопку «Удалить зону» внизу экрана.
