# Просмотр видео от носимого регистратора в Личном кабинете

Для просмотра необходимо зайти на сайт <https://camera.rt.ru/> и авторизоваться по логину и паролю аккаунта, в котором зарегистрирован носимый регистратор.

1. Откройте страницу носимого регистратора. 
2. Просматривайте прямой эфир. 
3. Нажмите на таймлайн, чтобы посмотреть видео с выбранного времени. При необходимости воспользуйтесь календарем.

## Просмотр локального архива носимого регистратора в Личном кабинете

Для получения видеоархива не с сервера платформы, а непосредственно из памяти носимого регистратора, нажмите на переключатель вверху плеера.
