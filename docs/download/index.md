# Ссылки для установки приложений, инструкции по установке и руководства пользователя

## Настольное приложение

Ссылки на дистрибутивы Настольного приложения:

- [сборка для 64-битных систем](/files/desktop-b2b/Rt.B2b.VideoClient-latest-x64.msi)
- [сборка для 32-битных систем](/files/desktop-b2b/Rt.B2b.VideoClient-latest-x86.msi)

Документация:

- [Инструкция по установке](/desktop_app/installation/) Настольного приложения
- [Руководство пользователя](/desktop_app/) Настольного приложения

## VMS-сервер

- [Инструкция по установке](/vms/vms_install_linux/) VMS-сервера на Linux
- [Руководство пользователя](/vms/) VMS-сервера

## Мобильные приложения

### Мобильное приложение для Android

- [Ссылка на приложение в Goolge Play](https://play.google.com/store/apps/details?id=ru.rt.videocomfort)

### Мобильное приложение для iOS

- [Ссылка на приложение в App Store](https://apps.apple.com/us/app/id1187243946)