# Функциональные характеристики

## Общие сведения

### Наименование и статус программного обеспечения

Наименование ПО: «Облачная платформа Видеонаблюдение Ростелеком».

Полное наименование ПО: «Облачная платформа Видеонаблюдение Ростелеком».

Статус ПО: введено в коммерческую эксплуатацию.

Сегмент бизнеса: B2B.

### Наименование разработчика и заказчика программного обеспечения

Заказчик: ПАО «Ростелеком».

Разработчик: ООО «Ростелеком информационные технологии».

## Функциональные характеристики

### Цели и назначение

«Облачная платформа Видеонаблюдение Ростелеком» (далее платформа) – это программное решение, которое предназначено для получения видеопотоков от клиентских устройств, анализа видеопотока программными средствами, для хранения и предоставления видеоархивов и событий, трансляции и управления потокового видео с камер, предоставления отчетов. Видеопотоки приходят в платформу по протоколу SPIF2 и RTSP. Подключаемые к платформе клиентские устройства должны обладать предустановленным ПО от ПАО «Ростелеком», либо предоставлять видеопоток посредством RTSP-протокола.

### Общие функциональные характеристики

Платформа обладает следующими функциональными характеристикам:

1. Администрирование:

    - ведение тарификационной системы (тарифные планы, тарифы, использование дополнительных услуг);
    - ведение реестров пользователей и устройств;
    - мониторинг текущего состояния пользовательских сессий;
    - мониторинг текущего состояния устройств;
    - внесение изменений в текущее состояние устройств, пользователей или услуг.

2. Предоставление клиентского доступа:

    - веб-портал с доступом к услуге Видеонаблюдение Ростелеком при помощи Личного кабинета;
    - мобильные приложения для ОС iOS и Android
    - настольное приложение Видеонаблюдение Ростелеком для ПК для ОС Windows;
    - приложение VMS сервер Видеонаблюдение Ростелеком.

3. Управление видеокамерами:

    - подключение/отключение клиентских видеокамер;
    - трансляция потокового видео;
    - при наличии — управление PTZ камер;
    - хранение видео (видеоархив);
    - предоставление доступа пользователю к прямому эфиру и архиву согласно тарифным настройкам;
    - управление битрейтом, режимом вещания, настройками звука, детекторами камеры;
    - управление видеокодеком;
    - управление качеством записи видео согласно тарифным настройкам на камере.

4. Фиксация и предоставление событий:

    - получение, хранение и отображение событий видеоаналитики, поступающих от детекторов камер и внешних модулей анализа;
    - показ событий пользователю с наложением на видеозапись;
    - настройка сценариев при получении события;
    - хранение событий согласно тарифным настройкам камеры.

5. Взаимодействие с внешними системами:

    - получение с видеокамер скриншотов и предоставление Яндексу полученных фотоизображений для контроля брендирования автомобилей;
    - получение событий от внешних провайдеров видеоаналитики;
    - установка геокоординат для камер с помощью сервиса Спутник.

6. Прочие функции:

    - управление каналами оповещений (SMS, Email, push-уведомления);
    - создание, сохранение и предоставление MP4-файлов, созданных из видеозаписей камер пользователя;
    - предоставление скриншотов;
    - формирование и предоставление ссылки на трансляцию камеры пользователя;
    - предоставление отчетов в XLSX-формате и в виде таблиц в веб-интерфейсе;
    - предоставление раскладок — показ видео с нескольких камер одновременно на одном экране.

## Принципы функционирования системы

В платформе реализованы три уровня взаимодействия:

- уровень веб-представлений;
- уровень серверов приложений;
- уровень формирования и хранения данных.

Общая схема взаимодействия компонентов платформы представлена на Рисунок 1.

![](./img/components_scheme.jpg)

_Рисунок 1. Общая схема взаимодействия компонентов платформы_

### Структура и функционирование системы

Платформа включает в себя функциональные модули (компоненты), предназначенные для решения соответствующих комплексов задач. Выделяются следующие функциональные модули (Рисунок 1):

- клиентские приложения;
- система взаимодействия с пользователями;
- система управления камерами;
- система обработки и доставки видео (видеотракт);
- система хранения архивного видео;
- система синхронизации видео;
- система оповещений;
- система событий;
- система отчётов.

Клиентские приложение предоставляют экранные формы для взаимодействия с пользователями. Клиентские приложения реализованы как для мобильных платформ iOS, Android, так и для операционных систем семейства Microsoft Windows.

Система взаимодействия с пользователями предоставляет программные интерфейсы для клиентских приложений, содержит в себе всю бизнес-логику и является основным компонентом платформы, управляющим сущностями предметной области.

Система управления камерами обеспечивает взаимодействие с камерами пользователей, управление параметрами камер, обновлением встроенного ПО, разработанного в ПАО «Ростелеком».

Система обработки и доставки видео выполняет приём, преобразование и тиражирование видеоконтента в требуемом клиенте формате.

Система хранения архивного видео обеспечивает хранение получаемого видеопотока и выгрузку архивных видеофрагментов.

Система синхронизации видео обеспечивает синхронизацию видеоархива между локальным архивом видеоисточника и централизованным архивом в системе хранения архивного видео в соответствие с установленными параметрами.

Система оповещений выполняет доставку оповещений клиентам различными способами, указанными в настройках пользователя. Доставка оповещений возможна как в клиентские приложения, так и посредством SMTP и SMS.

Система событий обеспечивает хранение событий, связанных с жизнедеятельностью платформы, а также событий от камер пользователей, событий внешней видеоаналитики.

Система отчётов предоставляет сведения об изменении объектов (записей о сотрудниках, событиях, камерах), а также другую информацию, сформированную в табличном виде.

В слое хранения данных для организации хранилищ используются следующие решения:

- хранилище объектов системы – PostgreSQL и Clickhouse;
- хранилище сессий – Redis;
- хранилище конфигураций камер - PostgreSQL;
- хранилище архивного видео – метаданные PostgreSQL и Redis, видеоархив – файловая система, предоставляемая ОС;
- хранилище событий – Apache Cassandra и Redis.

### Интеграционное взаимодействие

Системами-источниками данных являются внешние информационные системы: внешние провайдеры видеоаналитики, система аутентификации и идентификации «Ростелеком».

Интеграция осуществляется с использованием RESTful API платформ и/или HTTP API систем.

Для обеспечения технологии единого входа используются программные интерфейсы системы аутентификации и идентификации «Ростелеком». Все учётные записи пользователей ведутся на стороне системы аутентификации и идентификации. Платформа при аутентификации пользователей и администраторов осуществляет перенаправление запроса в систему аутентификации и идентификации. Результат проверки возвращается в виде ответа на HTTP-запрос c кодом операции, на основе которого осуществляется доступ в личный кабинет пользователя и к разделам настроек администраторов.

Для обеспечения интеграции с внешними провайдерами видеоаналитики используются получение заданий на обработку посредством брокера очередей и последующей отправкой заданий в выбранные провайдеры видеоаналитики с помощью HTTP-запросов.

Тарифицирование оказываемых услуг происходит следующим образом: платформа получает HTTP API запрос от системы предбиллинга Ростелеком (СУНОП) и отправляет в ответ JSON-файл с информацией о всех использованиях тарифов видео, SMS-пакетов и дополнительных услуг клиентами платформы. Расчет стоимости оказанных услуг и выставление счета осуществляется иными сервисами, данные в которые направляет система предбиллинга (СУНОП).

### Установка программного обеспечения

Установка программного обеспечения не требуется.

### Эксплуатационная документация

 * [Инструкция пользователя Личного кабинета](../account/about.md)
 * [Инструкция пользователя МП Android](../android/index.md)
 * [Инструкция пользователя МП iOS](../ios/index.md)