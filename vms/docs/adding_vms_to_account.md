# Добавление VMS-сервера в Личный кабинет «Видеонаблюдение Ростелеком»

1. Через браузер войдите в свой Личный кабинет «Видеонаблюдение Ростелеком» на сайте [lk-b2b.camera.rt.ru](https://lk-b2b.camera.rt.ru).
2. Нажмите «Добавить оборудование» в верхней части экрана.

    ![Экран списка камер: добавление оборудования](img/Online_Installation_28.png "Экран списка камер: добавление оборудования")
    
3. Выберите «VMS» из предлагаемого списка.

    ![Экран выбора оборудования](img/Online_Installation_29.png "Экран выбора оборудования")
    
4. Нажмите кнопку «Далее».

    ![Экран подготовки оборудования](img/Online_Installation_30.png "Экран подготовки оборудования")
    
5. В появившееся поле вставьте MAC-адрес вашего устройства и нажмите «Далее».

    ![Экран ввода данных оборудования](img/Online_Installation_31.png "Экран ввода данных оборудования")

    >  Для того, чтобы скопировать MAC-адрес вашего устройства, выполните шаги, указанные в разделе [«Начало работы»](./getting_started_with_vms.md).
    
6. Через несколько секунд VMS будет привязан к вашему Личному кабинету.

    ![Экран подключения VMS к ЛК](img/Online_Installation_32.png "Экран подключения VMS к ЛК")

7. На главной странице вашего Личного кабинета появился VMS. Его имя совпадает с MAC-адресом, который вы вводили при регистрации. Зайдите в его настройки, нажав на шестерёнку в правой верхней части окна (она появится при наведении мышки на окно).

    ![Экран списка камер](img/Online_Installation_33.png "Экран списка камер")
    
8. В пункте «Данные VMS» найдите строку с IP-адресом устройства. Выпишите его, так как он понадобится в дальнейшем.

    ![Экран с данными VMS](img/Online_Installation_34a.png "Экран с данными VMS")

9. В настройках вы можете также поменять название VMS.

    ![Экран настроек VMS](img/Online_Installation_34b.png "Экран настроек VMS")
