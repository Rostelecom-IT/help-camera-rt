# Начало работы с VMSBox

Перед началом работы с VMS-сервером вам понадобится узнать MAC-адрес устройства, который вы будете использовать для добавления VMS в Личный кабинет «Видеонаблюдение Ростелеком» [lk-b2b.camera.rt.ru](https://lk-b2b.camera.rt.ru).

Если вы работаете с приставкой **VMSbox**, вам необходимо:

1. Подключить приставку к питанию и локальной сети с выходом в интернет и наличием DHCP (см. [«Глоссарий»](./glossary.md)).

2. Зайти на роутер, который раздает DHCP и посмотреть, какой IP выдан для VMS.

3. При необходимости закрепить этот IP-адрес за MAC-адресом, чтобы IP-адрес не менялся. 

4. С любого ПК в локальной сети с VMSbox ввести в браузере `<ip_addr_vmsbox>:8080` — IP-адрес VMS-сервера.

5. В правом нижнем углу окна скопировать MAC-адрес для привязки в Личном кабинете.

6. По умолчанию для авторизации в веб-интерфейсе VMS-сервера используются следующие данные:

    > Логин: `admin`, Пароль `admin54321`.

7. Открыть Личный кабинет «Видеонаблюдение Ростелеком» [lk-b2b.camera.rt.ru](https://lk-b2b.camera.rt.ru) в новой вкладке браузера. 

8. Добавить VMS-сервер в Личный кабинет согласно 5 шагу инструкции [«Добавление VMS-сервера в Личный кабинет»](./adding_vms_to_account.md).