# Установка VMS-сервера на Linux 

## С доступом к сети интернет

Если ваша ОС из семейства Linux и **есть доступ** к сети интернет, установка происходит следующим образом.

1. При установке необходимо поставить 3 `deb`-пакета на персональный компьютер (ПК) с доступом в Интернет. 

2. Перед скачиванием необходимо убедиться в отсутствии на ПК `deb`-пакетов `videoserver-rtk-latest-amd64.deb`, `videoserver-rtk-prod-latest-amd64.deb`, `videoserver-rtk-web-prod-latest-amd64.deb` для того, чтобы не возникло ошибок при установке текущей версии VMS-сервера.

    ??? note "Пример (можно использовать любой из двух вариантов)"

        1. Открыть общее меню и ввести название файла в строке поиска:

            ![](./img/checking_vms_files.png)

            Повторить процедуру для все файлов.

        2. Открыть терминал и в командной строке ввести:

            ```
            find / -name videoserver-rtk-latest-amd64.deb
            ```

            Повторить команду для всех файлов.

3. Убедившись в их отсутствии, откройте терминал и в командной строке по порядку выполните следующие команды, нажимая **Enter** после ввода каждой из них:

    ```
    curl -O "https://help.camera.rt.ru/files/vms/videoserver-rtk-latest-amd64.deb"
    ```

    ```
    curl -O "https://help.camera.rt.ru/files/vms/videoserver-rtk-prod-latest-amd64.deb"
    ```
    
    ```
    curl -O "https://help.camera.rt.ru/files/vms/videoserver-rtk-web-prod-latest-amd64.deb"
    ```

4. Выполните следующую команду установки и нажмите **Enter**: 

    ```
    sudo dpkg -i videoserver*
    ```

5. Если VMS не удалось установить из-за отсутствия необходимых зависимостей, нужно выполнить следующую команду, нажимая **Enter**. Будет установлено все необходимое и сам VMS:

    ```
    sudo apt-get -f install
    ```

6. После установки необходимо перезагрузить Linux, или ввести данную команду для запуска ПО, нажимая **Enter**:

    ```
    sudo systemctl start videoserver-rtk
    ```   

    При перезагрузке Linux, ПО загружается автоматически.

7. Далее проверьте, что VMS-сервер установлен правильно, следуя инструкции [«Проверка установки VMS»](#проверка-установки-vms).

## Без доступа к сети интернет

Если же на вашем компьютере **нет доступа** к сети интернет, выполните следующие действия.

1. Скачайте 3 `deb`-пакета на персональный компьютер (ПК) с доступом в Интернет. Для этого в командной строке по порядку выполните следующие команды, нажимая **Enter** после каждой из них:

    ```
    curl -O "https://help.camera.rt.ru/files/vms/videoserver-rtk-latest-amd64.deb"
    ```

    ```
    curl -O "https://help.camera.rt.ru/files/vms/videoserver-rtk-prod-latest-amd64.deb"
    ```
    
    ```
    curl -O "https://help.camera.rt.ru/files/vms/videoserver-rtk-web-prod-latest-amd64.deb"
    ```

2. Перенесите скачанные файлы с расширением `.deb` на съемный носитель (USB-накопитель) в корневую директорию.

3. Необходимо подключить USB-накопитель к ПК с операционной системой Linux семейства Ubuntu, Debian, Mint.

4. Смонтируйте USB-накопитель при необходимости (в случае, если используется серверный дистрибутив Linux, Linux без GUI или без автомонтирования USB-носителей). Для этого следуйте инструкции «Монтирование на Linux USB-накопителя». 

    ??? note "Монтирование на Linux USB-накопителя"

        1. После **подключения USB к компьютеру** система добавит новое блочное устройство в каталог `/ dev /`. Чтобы проверить это, откройте терминал, введите следующую команду и нажмите **Enter**:

            ```
            $ sudo fdisk -l
            ```

            Полученный экран должен выглядеть следующим образом:

            ![](img/mount_usb_on_linux.png)

        2. После этого необходимо **создать точку монтирования**. Введите следующую команду, в которой `«sbd1»` относится к имени вашего USB-устройства, `«myusb»` — корневая директория с перенесёнными файлами, и нажмите **Enter**.

            ```
            $ mount /dev/sdb1 /myusb
            ```

        После выполнения этого шага вы успешно подключили USB-накопитель к вашей системе Linux. Чтобы проверить, что ваше USB-устройство установлено, введите команду `dh -H` и нажмите **Enter**. В последней строке вы увидите установленное устройство.

            $ df -H
            Filesystem                               Size  Used Avail Use% Mounted on
            devtmpfs                                 8.4G     0  8.4G   0% /dev
            tmpfs                                    8.4G  149M  8.2G   2% /dev/shm
            tmpfs                                    8.4G  2.0M  8.4G   1% /run
            /dev/mapper/fedora_localhost--live-root   74G   22G   49G  31% /
            tmpfs                                    8.4G  103k  8.4G   1% /tmp
            /dev/sda2                                1.1G  229M  725M  24% /boot
            /dev/mapper/fedora_localhost--live-home  152G   60G   85G  42% /home
            /dev/sda1                                210M   21M  189M  10% /boot/efi
            tmpfs                                    1.7G   14M  1.7G   1% /run/user/1000
            /dev/sdb1                                2.0G  4.1k  2.0G   1% /myusb


5. Откройте терминал.

6. Перейдите в корневую директорию накопителя, выполнив команду `cd <путь к корневой директории на USB-накопителе>`.

7. Выполните необходимые команды 4-6 шагов подраздела [«С доступом к сети интернет»](#с-доступом-к-сети-интернет) для установки и перезагрузки Linux.

8. Далее проверьте, что VMS-сервер установлен правильно, следуя инструкции [«Проверка установки VMS»](#проверка-установки-vms).

## Проверка установки VMS

1. Откройте веб-браузер, например, Internet Explorer.

2. Введите в адресной строке браузера <http://127.0.0.1:8080/>. По данной ссылке доступен интерфейс VMS-сервера, установленного локально на вашем ПК, в котором можно авторизоваться и работать с VMS-сервером.

    > Обратите внимание, первые символы должны быть именно `http`, а не `https`.

    ![Экран вкладки браузера](img/enter_local_ip_address_vms_v3.0.2-b318327.png "Экран вкладки браузера")

3. Если все сделано правильно, то в браузере появится интерфейс авторизации VMS-сервера.

![Интерфейс авторизации VMS-сервера](img/authorization_form_vms_v3.0.2-b318327.png "Интерфейс авторизации VMS-сервера")

4. По умолчанию для авторизации в веб-интерфейсе VMS-сервера используются следующие данные:

    > Логин: `admin`, Пароль `admin54321`.

Если пользователю нужно [удалить VMS](./delete_vms_linux.md), то его можно удалить при необходимости.

Если нужно [обновить VMS](./update_vms_linux.md), то скачайте новую версию и установите заново.